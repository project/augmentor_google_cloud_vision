# Google Cloud Vision Augmentor

Google Cloud Vision Augmentor is a submodule of Augmentor.
It provides an implementation of multiple Augmentor plugins to allow Augmentor to interface with Google Cloud Vision API.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/augmentor_google_cloud_vision).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/augmentor_google_cloud_vision).


## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers
- Sponsors


## Requirements

This module requires the following modules:

- [Augmentor](https://www.drupal.org/project/augmentor)
- [Google Cloud Vision for PHP](https://github.com/googleapis/google-cloud-php-vision)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

  * Configure the user permissions in Administration » People » Permissions:

    - Administer augmentors

      Users with this permission will see the web services > augmentors configuration list page. From here they can add, configure, delete, enable and disabled augmentors.

      Warning: Give to trusted roles only; this permission has security implications. Allows full administration access to create and edit augmentors.


## Troubleshooting

If you are not receiving data back for your user.

- Check the recent log messages report for exception messages.


## Maintainers

- Murray Woodman - [murrayw](https://www.drupal.org/u/murrayw)
- Eleo Basili - [eleonel](https://www.drupal.org/u/eleonel)
- Kelvin Wong - [KelvinWong](https://www.drupal.org/u/kelvinwong)
- Naveen Valecha - [naveenvalecha](https://www.drupal.org/u/naveenvalecha)


## Sponsors

- [Morpht Pty Ltd](https://www.morpht.com/)
  We are a team of dedicated and enthusiastic designers, programmers and site builders who know how to get the most from Drupal. We work for a variety of clients in government, education, media and pharmaceutical sectors.
